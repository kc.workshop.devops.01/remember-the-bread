import mock
import random
from src.business import tasks


_MOCK_ENTITIES = []


class KeyStub():

    def __init__(self, kind, id=None):
        self.kind = kind
        self.id = random.randint(100, 999) if id is None else id


class EntityStub(dict):

    def __init__(self, key=None):
        super(EntityStub, self).__init__()
        self.key = key


class IteratorStub():

    @property
    def pages(self):
        return iter([_MOCK_ENTITIES])


class QueryStub():

    def fetch(self, **kwargs):
        return IteratorStub()


class DatastoreClientStub():

    def key(self, kind, id=None):
        return KeyStub(kind, id=id)

    def query(self, **kwargs):
        return QueryStub()

    def put(self, entity):
        _MOCK_ENTITIES.append(entity)
        return entity

    def delete(self, key):
        entity = next((e for e in _MOCK_ENTITIES if e['id'] == key.id), None)
        if entity is not None:
            _MOCK_ENTITIES.remove(entity)


def setup_module():

    """ Create some entities for the testing """

    for _ in range(3):
        entity = EntityStub(key=KeyStub('Task'))
        entity['title'] = 'Task {}'.format(entity.key.id)
        _MOCK_ENTITIES.append(entity)


def test_from_datastore_success_1():

    """ Should return the same empty entity """

    data = {}
    entity = tasks._from_datastore(data)
    assert data is entity
    assert not data


def test_from_datastore_success_2():

    """ Should return an entity when a list is given """

    data = [1, '2', True, EntityStub(key=KeyStub('Task'))]
    entity = tasks._from_datastore(data)
    assert isinstance(entity, EntityStub)
    assert 'id' in entity


@mock.patch('google.cloud.datastore.Client', mock.Mock(return_value=DatastoreClientStub()))
def test_list_success():

    """ Should retrieve the tasks collection """

    entities = tasks.list()
    assert len(entities) == len(_MOCK_ENTITIES)
    if len(entities) > 0:
        entity = entities[0]
        assert 'id' in entity
        assert 'title' in entity


@mock.patch('google.cloud.datastore.Client', mock.Mock(return_value=DatastoreClientStub()))
def test_create_success():

    """ Should create a task """

    entities_desired_size = len(_MOCK_ENTITIES) + 1
    data = {'title': 'Task #1'}
    entity = tasks.create(data)
    assert 'id' in entity
    assert 'title' in entity
    assert entity['title'] == data['title']
    assert len(_MOCK_ENTITIES) == entities_desired_size


@mock.patch('google.cloud.datastore.Client', mock.Mock(return_value=DatastoreClientStub()))
def test_delete_success():

    """ Should delete a task """

    entities_desired_size = len(_MOCK_ENTITIES) - 1
    entity_id = _MOCK_ENTITIES[-1]['id']
    tasks.delete(entity_id)
    assert len(_MOCK_ENTITIES) == entities_desired_size
